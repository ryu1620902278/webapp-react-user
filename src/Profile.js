import React, {useEffect} from 'react';
import Typography from '@mui/material/Typography';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import Avatar from '@mui/material/Avatar';
import Box from '@mui/material/Box';
import TableCell from '@mui/material/TableCell';
import Button from '@mui/material/Button';



export default function Profile() {
  useEffect(() => {
      const token = localStorage.getItem('token')
      
    fetch('http://localhost:4444/authen', {
            method: 'POST',
            headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer '+token
            }
          })
            .then(response => response.json())
            .then(data => {
            if(data.status === 'ok'){
                //alert('Authen Success')
            }else{
                alert('Authen Failed')
                localStorage.removeItem('token')
                window.location = '/Login'

            }   
            })
              .catch((error) => {
              console.error('Error:', error);
            });
  }, [])
  const handleLogout = (event) => {
    event.preventDefault();
    window.location = '/Login'
  }
  const user = JSON.parse(localStorage.getItem('user'));

  

  return (
    <Card sx={{ minWidth: 275 }}>
      <CardContent>
      <TableCell align="center">
        <Box >
          <Avatar sx={{ width: 350, height: 350 }} src={user.picture} variant = "square"/>
                                
        </Box>
        <br></br>
      </TableCell>
        <Typography variant="h4" align="left"  >
          Welcome to {user.fname} {user.lname} 
        </Typography>
        <br></br>
        <Typography variant="h4" align="left" >
          Licenseplate: {user.licenseplate}
        </Typography>
        <br></br>
        <Typography variant="h4" align="left" >
          เวลาเข้าใช้งาน: {user.time}
        </Typography>
        <br></br>
        <Typography variant="h4" align="left" >
          วันที่เข้าใช้งาน: {user.date}
        </Typography>
        <br></br>
        <Button variant="contained" onClick={handleLogout}>Logout</Button>
      </CardContent>
      
    </Card>
  );
}